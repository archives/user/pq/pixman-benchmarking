upstream-round-1-1-g7ca1515

lowlevel-blt-bench: add src_x888_0565 (2014-03-20 12:40:56 +0200)
lowlevel-blt-bench: add in_reverse_8888_8888 test (2014-03-20 08:11:04 +0200)
lowlevel-blt-bench: over_reverse_n_8888 needs solid source (2014-03-20 08:11:04 +0200)
ARMv6: remove 1 instr per row in generate_composite_function (2014-03-20 08:08:58 +0200)
ARMv6: Fix indentation in the composite macros (2014-03-19 10:19:38 +0200)
Remove all the operators that use division from pixman-combine32.c (2014-01-04 16:13:27 -0500)
Copy the comments from pixman-combine32.c to pixman-combine-float.c (2014-01-04 16:13:27 -0500)
utils.c: Set DEVIATION to 0.0128 (2014-01-04 16:13:27 -0500)
Use floating point combiners for all operators that involve divisions (2014-01-04 16:13:27 -0500)
Soft Light: Consistent approach to division by zero (2014-01-04 16:13:27 -0500)
