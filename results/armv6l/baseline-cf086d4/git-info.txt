pixman-0.32.0-57-gcf086d4

cf086d4 MIPS: Drop #ifdef __ELF__ in definition of LEAF_MIPS32R2 (2015-05-07 12:49:09 +0300)
6f14bae test: Added more demos and tests to .gitignore file (2015-05-05 09:49:25 +0300)
e0c0153 test: Add a new benchmarker targeting affine operations (2015-04-24 10:25:42 +0300)
58e21d3 lowlevel-blt-bench: use a8r8g8b8 for CA solid masks (2015-04-20 16:18:18 +0300)
be49f92 lowlevel-blt-bench: use the test pattern parser (2015-04-15 12:43:01 +0300)
5b27912 lowlevel-blt-bench: add test name parser and self-test (2015-04-15 12:42:51 +0300)
1f45bd6 test/utils: add format aliases used by lowlevel-blt-bench (2015-04-15 12:42:45 +0300)
ef9c28a test/utils: add operator aliases for lowlevel-blt-bench (2015-04-15 12:42:40 +0300)
f1f6cc2 test/utils: support format name aliases (2015-04-15 12:42:33 +0300)
2c5fac9 test/utils: support operator name aliases (2015-04-15 12:41:47 +0300)
