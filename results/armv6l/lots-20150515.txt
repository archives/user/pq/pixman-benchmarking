Fri May 15 15:50:38 EEST 2015
+++ git reset --hard cf086d4949092861
HEAD is now at cf086d4 MIPS: Drop #ifdef __ELF__ in definition of LEAF_MIPS32R2
+++ make install
Making install in pixman
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
 /bin/mkdir -p '/home/pi/work/tmp/pixman-install/lib'
 /bin/bash ../libtool   --mode=install /usr/bin/install -c   libpixman-1.la '/home/pi/work/tmp/pixman-install/lib'
libtool: install: /usr/bin/install -c .libs/libpixman-1.so.0.33.1 /home/pi/work/tmp/pixman-install/lib/libpixman-1.so.0.33.1
libtool: install: (cd /home/pi/work/tmp/pixman-install/lib && { ln -s -f libpixman-1.so.0.33.1 libpixman-1.so.0 || { rm -f libpixman-1.so.0 && ln -s libpixman-1.so.0.33.1 libpixman-1.so.0; }; })
libtool: install: (cd /home/pi/work/tmp/pixman-install/lib && { ln -s -f libpixman-1.so.0.33.1 libpixman-1.so || { rm -f libpixman-1.so && ln -s libpixman-1.so.0.33.1 libpixman-1.so; }; })
libtool: install: /usr/bin/install -c .libs/libpixman-1.lai /home/pi/work/tmp/pixman-install/lib/libpixman-1.la
libtool: install: /usr/bin/install -c .libs/libpixman-1.a /home/pi/work/tmp/pixman-install/lib/libpixman-1.a
libtool: install: chmod 644 /home/pi/work/tmp/pixman-install/lib/libpixman-1.a
libtool: install: ranlib /home/pi/work/tmp/pixman-install/lib/libpixman-1.a
libtool: finish: PATH="/home/pi/work/tmp/pixman-install/bin:/home/pi/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games:/sbin" ldconfig -n /home/pi/work/tmp/pixman-install/lib
----------------------------------------------------------------------
Libraries have been installed in:
   /home/pi/work/tmp/pixman-install/lib

If you ever happen to want to link against installed libraries
in a given directory, LIBDIR, you must either use libtool, and
specify the full pathname of the library, or use the `-LLIBDIR'
flag during linking and do at least one of the following:
   - add LIBDIR to the `LD_LIBRARY_PATH' environment variable
     during execution
   - add LIBDIR to the `LD_RUN_PATH' environment variable
     during linking
   - use the `-Wl,-rpath -Wl,LIBDIR' linker flag
   - have your system administrator add LIBDIR to `/etc/ld.so.conf'

See any operating system documentation about shared libraries for
more information, such as the ld(1) and ld.so(8) manual pages.
----------------------------------------------------------------------
 /bin/mkdir -p '/home/pi/work/tmp/pixman-install/include/pixman-1'
 /usr/bin/install -c -m 644 pixman.h pixman-version.h '/home/pi/work/tmp/pixman-install/include/pixman-1'
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
Making install in demos
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[2]: Nothing to be done for 'install-exec-am'.
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
Making install in test
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[2]: Nothing to be done for 'install-exec-am'.
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[2]: Nothing to be done for 'install-exec-am'.
 /bin/mkdir -p '/home/pi/work/tmp/pixman-install/lib/pkgconfig'
 /usr/bin/install -c -m 644 pixman-1.pc '/home/pi/work/tmp/pixman-install/lib/pkgconfig'
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman'
Fri May 15 15:50:42 EEST 2015
+++ make check || echo "check fail ---- CONTINUE"
Making check in pixman
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
make[1]: Nothing to be done for 'check'.
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
Making check in demos
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[1]: Nothing to be done for 'check'.
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
Making check in test
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make  check-TESTS
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
PASS: oob-test
PASS: infinite-loop
PASS: trap-crasher
PASS: region-translate-test
PASS: fetch-test
PASS: a1-trap-test
PASS: prng-test
PASS: radial-invalid
PASS: pdf-op-test
PASS: region-test
PASS: combiner-test
PASS: scaling-crash-test
PASS: alpha-loop
PASS: scaling-helpers-test
PASS: thread-test
rotate test passed (checksum=81E9EC2F)
PASS: rotate-test
PASS: alphamap
PASS: gradient-crash-test
PASS: pixel-test
matrix test passed (checksum=BEBF98C3)
PASS: matrix-test
composite traps test passed (checksum=AF41D210)
PASS: composite-traps-test
region_contains test passed (checksum=548E0F3F)
PASS: region-contains-test
glyph test passed (checksum=FA478A79)
PASS: glyph-test
PASS: stress-test
blitters test passed (checksum=CC21DDF0)
PASS: blitters-test
affine test passed (checksum=BE724CFE)
PASS: affine-test
scaling test passed (checksum=92E0F068)
PASS: scaling-test
PASS: composite
PASS: tolerance-test
===================
All 29 tests passed
===================
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman'
Fri May 15 17:11:30 EEST 2015
+++ TESTS=over_n_8888 over_n_0565 in_8888_8 over_8888_8_0565 over_8888_n_0565 in_n_8888 over_8888_8888 in_8888_8 in_n_8888 in_reverse_8888_8888 src_1555_8888 src_8888_8888 src_0565_0565 ../pixman-benchmarking/run-series.bash
Running over_n_8888...

real	29m23.062s
user	29m2.230s
sys	0m13.010s
Running over_n_0565...

real	34m32.528s
user	34m12.430s
sys	0m12.750s
Running in_8888_8...

real	31m39.036s
user	31m16.210s
sys	0m12.520s
Running over_8888_8_0565...

real	51m23.313s
user	51m0.400s
sys	0m13.340s
Running over_8888_n_0565...

real	47m25.674s
user	47m2.450s
sys	0m13.230s
Running in_n_8888...

real	24m37.900s
user	24m19.170s
sys	0m12.160s
Running over_8888_8888...

real	19m2.229s
user	18m43.830s
sys	0m12.200s
Running in_8888_8...

real	31m36.848s
user	31m16.280s
sys	0m12.490s
Running in_n_8888...

real	24m39.789s
user	24m20.790s
sys	0m12.470s
Running in_reverse_8888_8888...

real	22m2.501s
user	21m41.020s
sys	0m12.450s
Running src_1555_8888...

real	23m9.386s
user	22m50.200s
sys	0m12.790s
Running src_8888_8888...

real	14m37.455s
user	14m19.600s
sys	0m12.090s
Running src_0565_0565...

real	15m0.346s
user	14m40.100s
sys	0m12.850s
Fri May 15 23:20:41 EEST 2015
+++ git reset --hard 238b57609e7b9b6e
HEAD is now at 238b576 pixman-fast-path: Add over_n_0565 fast path
+++ make install
Making install in pixman
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
  CC     pixman-fast-path.lo
  CCLD   libpixman-1.la
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
 /bin/mkdir -p '/home/pi/work/tmp/pixman-install/lib'
 /bin/bash ../libtool   --mode=install /usr/bin/install -c   libpixman-1.la '/home/pi/work/tmp/pixman-install/lib'
libtool: install: /usr/bin/install -c .libs/libpixman-1.so.0.33.1 /home/pi/work/tmp/pixman-install/lib/libpixman-1.so.0.33.1
libtool: install: (cd /home/pi/work/tmp/pixman-install/lib && { ln -s -f libpixman-1.so.0.33.1 libpixman-1.so.0 || { rm -f libpixman-1.so.0 && ln -s libpixman-1.so.0.33.1 libpixman-1.so.0; }; })
libtool: install: (cd /home/pi/work/tmp/pixman-install/lib && { ln -s -f libpixman-1.so.0.33.1 libpixman-1.so || { rm -f libpixman-1.so && ln -s libpixman-1.so.0.33.1 libpixman-1.so; }; })
libtool: install: /usr/bin/install -c .libs/libpixman-1.lai /home/pi/work/tmp/pixman-install/lib/libpixman-1.la
libtool: install: /usr/bin/install -c .libs/libpixman-1.a /home/pi/work/tmp/pixman-install/lib/libpixman-1.a
libtool: install: chmod 644 /home/pi/work/tmp/pixman-install/lib/libpixman-1.a
libtool: install: ranlib /home/pi/work/tmp/pixman-install/lib/libpixman-1.a
libtool: finish: PATH="/home/pi/work/tmp/pixman-install/bin:/home/pi/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games:/sbin" ldconfig -n /home/pi/work/tmp/pixman-install/lib
----------------------------------------------------------------------
Libraries have been installed in:
   /home/pi/work/tmp/pixman-install/lib

If you ever happen to want to link against installed libraries
in a given directory, LIBDIR, you must either use libtool, and
specify the full pathname of the library, or use the `-LLIBDIR'
flag during linking and do at least one of the following:
   - add LIBDIR to the `LD_LIBRARY_PATH' environment variable
     during execution
   - add LIBDIR to the `LD_RUN_PATH' environment variable
     during linking
   - use the `-Wl,-rpath -Wl,LIBDIR' linker flag
   - have your system administrator add LIBDIR to `/etc/ld.so.conf'

See any operating system documentation about shared libraries for
more information, such as the ld(1) and ld.so(8) manual pages.
----------------------------------------------------------------------
 /bin/mkdir -p '/home/pi/work/tmp/pixman-install/include/pixman-1'
 /usr/bin/install -c -m 644 pixman.h pixman-version.h '/home/pi/work/tmp/pixman-install/include/pixman-1'
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
Making install in demos
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[2]: Nothing to be done for 'install-exec-am'.
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
Making install in test
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
  CCLD   oob-test
  CCLD   infinite-loop
  CCLD   trap-crasher
  CCLD   region-translate-test
  CCLD   fetch-test
  CCLD   a1-trap-test
  CCLD   prng-test
  CCLD   radial-invalid
  CCLD   pdf-op-test
  CCLD   region-test
  CCLD   combiner-test
  CCLD   scaling-crash-test
  CCLD   alpha-loop
  CCLD   scaling-helpers-test
  CCLD   thread-test
  CCLD   rotate-test
  CCLD   alphamap
  CCLD   gradient-crash-test
  CCLD   pixel-test
  CCLD   matrix-test
  CCLD   composite-traps-test
  CCLD   region-contains-test
  CCLD   glyph-test
  CCLD   stress-test
  CCLD   blitters-test
  CCLD   affine-test
  CCLD   scaling-test
  CCLD   composite
  CCLD   tolerance-test
  CCLD   lowlevel-blt-bench
  CCLD   radial-perf-test
  CCLD   check-formats
  CCLD   scaling-bench
  CCLD   affine-bench
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[2]: Nothing to be done for 'install-exec-am'.
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[2]: Nothing to be done for 'install-exec-am'.
 /bin/mkdir -p '/home/pi/work/tmp/pixman-install/lib/pkgconfig'
 /usr/bin/install -c -m 644 pixman-1.pc '/home/pi/work/tmp/pixman-install/lib/pkgconfig'
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman'
Fri May 15 23:26:35 EEST 2015
+++ make check || echo "check fail ---- CONTINUE"
Making check in pixman
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
make[1]: Nothing to be done for 'check'.
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
Making check in demos
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[1]: Nothing to be done for 'check'.
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
Making check in test
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make  check-TESTS
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
PASS: oob-test
PASS: infinite-loop
PASS: trap-crasher
PASS: region-translate-test
PASS: fetch-test
PASS: a1-trap-test
PASS: prng-test
PASS: radial-invalid
PASS: pdf-op-test
PASS: region-test
PASS: combiner-test
PASS: scaling-crash-test
PASS: alpha-loop
PASS: scaling-helpers-test
PASS: thread-test
rotate test passed (checksum=81E9EC2F)
PASS: rotate-test
PASS: alphamap
PASS: gradient-crash-test
PASS: pixel-test
matrix test passed (checksum=BEBF98C3)
PASS: matrix-test
composite traps test passed (checksum=AF41D210)
PASS: composite-traps-test
region_contains test passed (checksum=548E0F3F)
PASS: region-contains-test
glyph test passed (checksum=FA478A79)
PASS: glyph-test
PASS: stress-test
blitters test passed (checksum=CC21DDF0)
PASS: blitters-test
affine test passed (checksum=BE724CFE)
PASS: affine-test
scaling test passed (checksum=92E0F068)
PASS: scaling-test
PASS: composite
PASS: tolerance-test
===================
All 29 tests passed
===================
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman'
Sat May 16 00:46:21 EEST 2015
+++ TESTS=over_n_8888 over_n_0565 in_8888_8 over_8888_8_0565 over_8888_n_0565 in_n_8888 over_8888_8888 in_8888_8 in_n_8888 in_reverse_8888_8888 src_1555_8888 src_8888_8888 src_0565_0565 ../pixman-benchmarking/run-series.bash
Running over_n_8888...

real	23m15.750s
user	22m56.590s
sys	0m12.590s
Running over_n_0565...

real	28m14.278s
user	27m53.690s
sys	0m12.750s
Running in_8888_8...

real	32m22.542s
user	32m0.420s
sys	0m12.560s
Running over_8888_8_0565...

real	51m21.501s
user	50m58.320s
sys	0m13.550s
Running over_8888_n_0565...

real	47m26.772s
user	47m2.580s
sys	0m13.020s
Running in_n_8888...

real	24m37.894s
user	24m18.940s
sys	0m12.360s
Running over_8888_8888...

real	19m4.055s
user	18m43.030s
sys	0m12.540s
Running in_8888_8...

real	32m19.094s
user	31m58.940s
sys	0m12.990s
Running in_n_8888...

real	24m39.781s
user	24m16.750s
sys	0m12.540s
Running in_reverse_8888_8888...

real	21m58.539s
user	21m39.690s
sys	0m12.540s
Running src_1555_8888...

real	23m6.845s
user	22m47.760s
sys	0m12.680s
Running src_8888_8888...

real	14m49.146s
user	14m15.960s
sys	0m12.440s
Running src_0565_0565...

real	14m57.134s
user	14m38.900s
sys	0m12.440s
Sat May 16 06:44:35 EEST 2015
+++ git reset --hard erebos-master
HEAD is now at 5ac6d12 armv6: Add over_n_0565 fast path
+++ make install
Making install in pixman
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
  CC     pixman-arm-simd.lo
  CPPAS  pixman-arm-simd-asm.lo
  CCLD   libpixman-arm-simd.la
  CCLD   libpixman-1.la
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
 /bin/mkdir -p '/home/pi/work/tmp/pixman-install/lib'
 /bin/bash ../libtool   --mode=install /usr/bin/install -c   libpixman-1.la '/home/pi/work/tmp/pixman-install/lib'
libtool: install: /usr/bin/install -c .libs/libpixman-1.so.0.33.1 /home/pi/work/tmp/pixman-install/lib/libpixman-1.so.0.33.1
libtool: install: (cd /home/pi/work/tmp/pixman-install/lib && { ln -s -f libpixman-1.so.0.33.1 libpixman-1.so.0 || { rm -f libpixman-1.so.0 && ln -s libpixman-1.so.0.33.1 libpixman-1.so.0; }; })
libtool: install: (cd /home/pi/work/tmp/pixman-install/lib && { ln -s -f libpixman-1.so.0.33.1 libpixman-1.so || { rm -f libpixman-1.so && ln -s libpixman-1.so.0.33.1 libpixman-1.so; }; })
libtool: install: /usr/bin/install -c .libs/libpixman-1.lai /home/pi/work/tmp/pixman-install/lib/libpixman-1.la
libtool: install: /usr/bin/install -c .libs/libpixman-1.a /home/pi/work/tmp/pixman-install/lib/libpixman-1.a
libtool: install: chmod 644 /home/pi/work/tmp/pixman-install/lib/libpixman-1.a
libtool: install: ranlib /home/pi/work/tmp/pixman-install/lib/libpixman-1.a
libtool: finish: PATH="/home/pi/work/tmp/pixman-install/bin:/home/pi/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games:/sbin" ldconfig -n /home/pi/work/tmp/pixman-install/lib
----------------------------------------------------------------------
Libraries have been installed in:
   /home/pi/work/tmp/pixman-install/lib

If you ever happen to want to link against installed libraries
in a given directory, LIBDIR, you must either use libtool, and
specify the full pathname of the library, or use the `-LLIBDIR'
flag during linking and do at least one of the following:
   - add LIBDIR to the `LD_LIBRARY_PATH' environment variable
     during execution
   - add LIBDIR to the `LD_RUN_PATH' environment variable
     during linking
   - use the `-Wl,-rpath -Wl,LIBDIR' linker flag
   - have your system administrator add LIBDIR to `/etc/ld.so.conf'

See any operating system documentation about shared libraries for
more information, such as the ld(1) and ld.so(8) manual pages.
----------------------------------------------------------------------
 /bin/mkdir -p '/home/pi/work/tmp/pixman-install/include/pixman-1'
 /usr/bin/install -c -m 644 pixman.h pixman-version.h '/home/pi/work/tmp/pixman-install/include/pixman-1'
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
Making install in demos
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[2]: Nothing to be done for 'install-exec-am'.
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
Making install in test
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
  CCLD   oob-test
  CCLD   infinite-loop
  CCLD   trap-crasher
  CCLD   region-translate-test
  CCLD   fetch-test
  CCLD   a1-trap-test
  CCLD   prng-test
  CCLD   radial-invalid
  CCLD   pdf-op-test
  CCLD   region-test
  CCLD   combiner-test
  CCLD   scaling-crash-test
  CCLD   alpha-loop
  CCLD   scaling-helpers-test
  CCLD   thread-test
  CCLD   rotate-test
  CCLD   alphamap
  CCLD   gradient-crash-test
  CCLD   pixel-test
  CCLD   matrix-test
  CCLD   composite-traps-test
  CCLD   region-contains-test
  CCLD   glyph-test
  CCLD   stress-test
  CCLD   blitters-test
  CCLD   affine-test
  CCLD   scaling-test
  CCLD   composite
  CCLD   tolerance-test
  CCLD   lowlevel-blt-bench
  CCLD   radial-perf-test
  CCLD   check-formats
  CCLD   scaling-bench
  CCLD   affine-bench
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[2]: Nothing to be done for 'install-exec-am'.
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[2]: Nothing to be done for 'install-exec-am'.
 /bin/mkdir -p '/home/pi/work/tmp/pixman-install/lib/pkgconfig'
 /usr/bin/install -c -m 644 pixman-1.pc '/home/pi/work/tmp/pixman-install/lib/pkgconfig'
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman'
Sat May 16 06:47:23 EEST 2015
+++ make check || echo "check fail ---- CONTINUE"
Making check in pixman
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
make[1]: Nothing to be done for 'check'.
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/pixman'
Making check in demos
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
make[1]: Nothing to be done for 'check'.
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/demos'
Making check in test
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make  check-TESTS
make[2]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
PASS: oob-test
PASS: infinite-loop
PASS: trap-crasher
PASS: region-translate-test
PASS: fetch-test
PASS: a1-trap-test
PASS: prng-test
PASS: radial-invalid
PASS: pdf-op-test
PASS: region-test
PASS: combiner-test
PASS: scaling-crash-test
PASS: alpha-loop
PASS: scaling-helpers-test
PASS: thread-test
rotate test passed (checksum=81E9EC2F)
PASS: rotate-test
PASS: alphamap
PASS: gradient-crash-test
PASS: pixel-test
matrix test passed (checksum=BEBF98C3)
PASS: matrix-test
composite traps test passed (checksum=AF41D210)
PASS: composite-traps-test
region_contains test passed (checksum=548E0F3F)
PASS: region-contains-test
glyph test passed (checksum=FA478A79)
PASS: glyph-test
PASS: stress-test
blitters test passed (checksum=CC21DDF0)
PASS: blitters-test
affine test passed (checksum=BE724CFE)
PASS: affine-test
scaling test passed (checksum=92E0F068)
PASS: scaling-test
PASS: composite
PASS: tolerance-test
===================
All 29 tests passed
===================
make[2]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman/test'
make[1]: Entering directory '/mnt/usb-extra/rpi1/work/git/pixman'
make[1]: Leaving directory '/mnt/usb-extra/rpi1/work/git/pixman'
Sat May 16 08:07:47 EEST 2015
+++ TESTS=over_n_8888 over_n_0565 in_8888_8 over_8888_8_0565 over_8888_n_0565 in_n_8888 over_8888_8888 in_8888_8 in_n_8888 in_reverse_8888_8888 src_1555_8888 src_8888_8888 src_0565_0565 ../pixman-benchmarking/run-series.bash
Running over_n_8888...

real	16m25.271s
user	16m5.050s
sys	0m12.440s
Running over_n_0565...

real	16m47.004s
user	16m28.030s
sys	0m13.020s
Running in_8888_8...

real	31m27.607s
user	31m7.610s
sys	0m12.910s
Running over_8888_8_0565...

real	51m16.335s
user	50m50.950s
sys	0m13.460s
Running over_8888_n_0565...

real	47m21.681s
user	46m58.890s
sys	0m12.810s
Running in_n_8888...

real	24m32.624s
user	24m13.110s
sys	0m12.940s
Running over_8888_8888...

real	19m2.981s
user	18m41.790s
sys	0m12.140s
Running in_8888_8...

real	31m29.625s
user	31m10.050s
sys	0m12.460s
Running in_n_8888...

real	24m34.907s
user	24m14.600s
sys	0m12.040s
Running in_reverse_8888_8888...

real	22m1.256s
user	21m40.890s
sys	0m12.070s
Running src_1555_8888...

real	23m4.808s
user	22m46.190s
sys	0m12.210s
Running src_8888_8888...

real	14m36.950s
user	14m15.770s
sys	0m12.110s
Running src_0565_0565...

real	14m58.635s
user	14m40.330s
sys	0m12.510s
Sat May 16 13:45:27 EEST 2015
