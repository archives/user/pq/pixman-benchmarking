pixman-0.32.0-26-g36f487d

36f487d ARMv6: Add fast path for over_reverse_n_8888 (2014-03-28 10:58:07 +0200)
c343846 lowlevel-blt-bench: add in_reverse_8888_8888 test (2014-03-20 08:33:05 -0400)
898859f lowlevel-blt-bench: over_reverse_n_8888 needs solid source (2014-03-20 08:33:05 -0400)
38317cb ARMv6: remove 1 instr per row in generate_composite_function (2014-03-20 08:33:05 -0400)
763a6d3 ARMv6: Fix indentation in the composite macros (2014-03-20 08:33:05 -0400)
82d0946 Remove all the operators that use division from pixman-combine32.c (2014-01-04 16:13:27 -0500)
ccb1df0 Copy the comments from pixman-combine32.c to pixman-combine-float.c (2014-01-04 16:13:27 -0500)
94244b0 utils.c: Set DEVIATION to 0.0128 (2014-01-04 16:13:27 -0500)
15aa37a Use floating point combiners for all operators that involve divisions (2014-01-04 16:13:27 -0500)
8f38243 Soft Light: Consistent approach to division by zero (2014-01-04 16:13:27 -0500)
