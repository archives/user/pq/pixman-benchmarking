pixman-0.32.0-81-g355a3d0

355a3d0 llbb: HACK to run only L1 test (2015-07-02 11:52:49 +0300)
2e31be1 armv6: set up the nops experiment with a macro (2015-07-02 11:49:50 +0300)
f6b5cb8 armv6: Add over_n_0565 fast path (2015-06-24 13:54:35 +0300)
7a5dc05 armv6: Add over_n_8888 fast path (2015-06-24 13:54:35 +0300)
5067804 pixman-fast-path: Add over_n_0565 fast path (2015-06-24 13:54:35 +0300)
c5083a3 pixman-fast-path: Add over_n_8888 fast path (2015-06-24 13:54:35 +0300)
853eba7 The Pixman alignment patch (2015-06-24 13:54:11 +0300)
75a7f72 lowlevel-blt-bench: warm up Pixman operation cache (2015-06-24 13:45:32 +0300)
11d12bb lowlevel-blt-bench: add option to skip memcpy measurement (2015-06-10 13:54:01 +0300)
1550d62 lowlevel-blt-bench: add CSV output mode (2015-06-10 13:30:31 +0300)

configure options: CPPFLAGS=-DPROFILING_NOPS=216
