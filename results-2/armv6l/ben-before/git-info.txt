pixman-0.32.0-84-g6d99069

6d99069 ARMv6: Force fast paths to have fixed alignment to the BTAC (2015-05-26 16:44:52 +0100)
3605dde armv6: Add fetcher for a8 bilinear-interpolation scaled images (2015-05-07 22:55:34 +0100)
703c2dc armv6: Add fetcher for r5g6b5 bilinear-interpolation scaled images (2015-05-07 22:55:34 +0100)
07e277a armv6: Add fetcher for x8r8g8b8 bilinear-interpolation scaled images (2015-05-07 22:55:34 +0100)
086bd34 armv6: Add fetcher for a8r8g8b8 bilinear-interpolation scaled images (2015-05-07 22:55:34 +0100)
7b0d8c8 armv6: Add fetcher for a8 nearest-neighbour transformed images (2015-05-07 22:55:33 +0100)
c1fa248 armv6: Add fetcher for x8r8g8b8 nearest-neighbour transformed images (2015-05-07 22:55:33 +0100)
aa14172 armv6: Add fetcher for r5g6b5 nearest-neighbour transformed images (2015-05-07 22:55:33 +0100)
cc6ebd5 armv6: Add fetcher for a8r8g8b8 nearest-neighbour transformed images (2015-05-07 22:55:33 +0100)
7713b50 armv6: Add optimised scanline fetcher for a1r5g5b5 (2015-05-07 22:55:33 +0100)
