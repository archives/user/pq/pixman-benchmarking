#include <stdio.h> /*
set -x
gcc -Wall -Wextra -Wno-unused-parameter -O2 $0 -o ${0%.c} -lrt
exit
*/

/*
 * Copyright © 2015 Raspberry Pi Foundation
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of the copyright holders not be used in
 * advertising or publicity pertaining to distribution of the software without
 * specific, written prior permission.  The copyright holders make no
 * representations about the suitability of this software for any purpose.  It
 * is provided "as is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 * OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdint.h>
#include <signal.h>

#define NSEC_PER_SEC 1000000000

/*
 * gettimeofday
 * clock_gettime: monotonic and process
 * getrusage
 * clock
 */

struct stamp {
	struct timespec cgt_mono; /* clock_gettime(MONOTONIC) */
	struct timespec cgt_proc; /* clock_gettime(PROCESS) */
	struct timespec timeofday; /* gettimeofday */
	struct timespec rusage_user; /* getrusage -> ru_utime */
	struct timespec rusage_sys; /* getrusage -> ru_stime */
	struct timespec clock; /* clock() */
};

static void
get_clock_gettime_monotonic(struct timespec *ts)
{
	clock_gettime(CLOCK_MONOTONIC_RAW, ts);
}

static void
get_clock_gettime_process(struct timespec *ts)
{
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, ts);
}

static void
timespec_from_timeval(struct timespec *ts, const struct timeval *tv)
{
	ts->tv_sec = tv->tv_sec;
	ts->tv_nsec = tv->tv_usec * 1000;
}

static void
get_gettimeofday(struct timespec *ts)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	timespec_from_timeval(ts, &tv);
}

static void
get_getrusage(struct timespec *user, struct timespec *sys)
{
	struct rusage r;

	getrusage(RUSAGE_SELF, &r);
	timespec_from_timeval(user, &r.ru_utime);
	timespec_from_timeval(sys, &r.ru_stime);
}

static void
get_clock(struct timespec *ts)
{
	ldiv_t q;
	clock_t t;

	t = clock();

	q = ldiv(t, CLOCKS_PER_SEC);
	ts->tv_sec = q.quot;
	ts->tv_nsec = q.rem * NSEC_PER_SEC / CLOCKS_PER_SEC;
}

static void
sample_times(struct stamp *s)
{
	get_clock_gettime_monotonic(&s->cgt_mono);
	get_clock_gettime_process(&s->cgt_proc);
	get_gettimeofday(&s->timeofday);
	get_getrusage(&s->rusage_user, &s->rusage_sys);
	get_clock(&s->clock);
}

static int64_t
timespec_sub_to_nsec(const struct timespec *a, const struct timespec *b)
{
	int64_t d;

	d = a->tv_sec - b->tv_sec;
	d *= NSEC_PER_SEC;
	d += a->tv_nsec - b->tv_nsec;

	return d;
}

static int
nsec_to_msec(int64_t nsec)
{
	return nsec / 1000000;
}

static void
print_elapsed(const struct stamp *from, const struct stamp *to)
{
	int64_t cgt_mono;
	int64_t cgt_proc;
	int64_t timeofday;
	int64_t rusage_user;
	int64_t rusage_sys;
	int64_t clocktime;

	cgt_mono = timespec_sub_to_nsec(&to->cgt_mono, &from->cgt_mono);
	cgt_proc = timespec_sub_to_nsec(&to->cgt_proc, &from->cgt_proc);
	timeofday = timespec_sub_to_nsec(&to->timeofday, &from->timeofday);
	rusage_user = timespec_sub_to_nsec(&to->rusage_user,
					   &from->rusage_user);
	rusage_sys = timespec_sub_to_nsec(&to->rusage_sys, &from->rusage_sys);
	clocktime = timespec_sub_to_nsec(&to->clock, &from->clock);

	printf("%5d %5d %5d %5d %5d %5d\n",
		nsec_to_msec(cgt_mono),
		nsec_to_msec(cgt_proc),
		nsec_to_msec(timeofday),
		nsec_to_msec(rusage_user),
		nsec_to_msec(rusage_sys),
		nsec_to_msec(clocktime));
}

/* Copied from Pixman lowlevel-blt-bench.c. */

#define BUFSIZE (1920 * 1080 * 4)

struct work {
	uint32_t *src;
	uint32_t *dst;
	int n;
};

static void
work_hard(struct work *w)
{
	int i;

	for (i = 0; i < w->n; i++) {
		memcpy(w->src, w->dst, BUFSIZE);
		memcpy(w->dst, w->src, BUFSIZE);
	}
}

static void
work_init(struct work *w)
{
	int i = 0;
	struct timespec begin;
	struct timespec end;

	w->src = malloc(BUFSIZE);
	w->dst = malloc(BUFSIZE);
	w->n = 1;
	memset(w->src, 0xC1, BUFSIZE);

	get_clock_gettime_monotonic(&begin);
	do {
		work_hard(w);
		get_clock_gettime_monotonic(&end);
		i++;
	} while (timespec_sub_to_nsec(&end, &begin) <
		 (int64_t)3 * NSEC_PER_SEC);

	w->n = i;
	printf("iteration count: %d\n", w->n);
}

static void
work_fini(struct work *w)
{
	free(w->src);
	free(w->dst);
}

static volatile sig_atomic_t running = 1;

static void
signal_intr(int signum)
{
	running = 0;
}

static void
catch_intr(void)
{
	struct sigaction sigint;

	sigint.sa_handler = signal_intr;
	sigemptyset(&sigint.sa_mask);
	sigint.sa_flags = SA_RESETHAND;
	sigaction(SIGINT, &sigint, NULL);
}

int
main(int argc, char *argv[])
{
	struct work work;
	struct stamp begin;
	struct stamp end;

	catch_intr();

	work_init(&work);

	printf("            milliseconds\n");
	printf(" mono  proc tmday  user   sys   clk\n");
	while (running) {
		fflush(stdout);
		sample_times(&begin);
		work_hard(&work);
		sample_times(&end);
		print_elapsed(&begin, &end);
	}

	work_fini(&work);
	return 0;
}
