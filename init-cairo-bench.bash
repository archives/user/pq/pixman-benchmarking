#!/bin/bash

BENCHDIR="$(readlink -en $(dirname $0))"
MAKEOPTS="-j4"

set -e

source "$BENCHDIR/cairo-env-setup.bash"

set -x

mkdir -p "$BASEDIR"/{pixman,cairo}

pushd "$BASEDIR/pixman"
~/work/git/pixman/autogen.sh --prefix=$PFX --disable-gtk CFLAGS='-O2 -g'
make $MAKEOPTS install
popd

pushd "$BASEDIR/cairo"
~/work/git/cairo/autogen.sh --prefix=$PFX CFLAGS='-O2 -g' CXXFLAGS='-O2 -g'
make $MAKEOPTS install
pushd perf
make cairo-perf-diff-files
popd
popd

pushd ~/work/git/trimmed-cairo-traces
make clean
make
popd

ln -sf "$BASEDIR/cairo/perf/.libs/cairo-perf-trace" "$PFX/bin/"
ln -sf "$BASEDIR/cairo/perf/.libs/cairo-perf-diff-files" "$PFX/bin/"
