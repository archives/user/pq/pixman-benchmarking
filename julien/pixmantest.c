/*
 * Copyright (C) 2014 Julien Isorce <julien.isorce@collabora.co.uk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/* gcc -Wall pixmantest.c -o pixmantest $(pkg-config --cflags --libs glib-2.0 pixman-1) */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <pixman.h>

/*
pixman_bool_t pixman_blt                (uint32_t           *src_bits,
           uint32_t           *dst_bits,
           int                 src_stride, // in 32-bit words
           int                 dst_stride, // in 32-bit words
           int                 src_bpp,
           int                 dst_bpp,
           int                 src_x,
           int                 src_y,
           int                 dest_x,
           int                 dest_y,
           int                 width,
           int                 height);
*/

int main (int argc, char *argv[])
{
  gint64 t0 = 0;
  gint64 tf = 0;
  double deltatime = 0;
  int width = 2080; /* 2080 / 16.0 = 130.0 */
  int height = 1200;
  int bpp = 16;
  int stride = width * 2;
  int nb_iter = 0;
  int i = 0;
  int fps = 0;

  uint32_t *src_bits = g_malloc0 (stride * height);
  uint32_t *dst_bits = g_malloc0 (stride * height);

  t0 = g_get_monotonic_time ();

  nb_iter = 1000;
  for (i = 0; i < nb_iter; ++i) {
    /* one frame iteration */
      /* will call pixman_composite_src_0565_0565_asm_armv6 on RPI */
      pixman_blt (dst_bits, src_bits, stride / 4, stride / 4, bpp, bpp, 10, 10, 20, 20, 1920, 1080);
      pixman_blt (dst_bits, src_bits, stride / 4, stride / 4, bpp, bpp, 10, 10, 20, 20, 1920, 1080);
  }

  tf = g_get_monotonic_time ();

  deltatime = (tf - t0) / (1000.0 * 1000.0);

  fps = (int) (((double) nb_iter) / deltatime);

  g_print ("time %f s, speed %f MB/s -- fps: %d\n", deltatime, 2 * 1920.0*1080.0*2*nb_iter / deltatime / 1024.0 / 1024.0, fps);

  g_free (src_bits);
  g_free (dst_bits);

  g_print ("-- end\n");

  return 0;
}
