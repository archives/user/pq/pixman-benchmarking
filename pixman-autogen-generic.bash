#!/bin/bash

set -x

./autogen.sh --prefix=$PFX \
	--disable-openmp \
	--disable-loongson-mmi \
	--disable-mmx \
	--disable-sse2 \
	--disable-ssse3 \
	--disable-vmx \
	--disable-arm-simd \
	--disable-arm-neon \
	--disable-arm-iwmmxt \
	--disable-arm-iwmmxt2 \
	--disable-mips-dspr2 \
	--disable-gcc-inline-asm \
	--enable-static-testprogs \
	--disable-gtk \
	--disable-libpng \
	--program-suffix=.generic

