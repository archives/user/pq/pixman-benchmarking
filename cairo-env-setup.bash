function concatenate_colon {
	local IFS=':'
	echo "$*"
}

function add_export_env {
	local VAR="$1"
	shift
	local VAL=$(eval echo "\$$VAR")
	if [ "$VAL" ]; then
		VAL=$(concatenate_colon "$@" "$VAL")
	else
		VAL=$(concatenate_colon "$@")
	fi
	eval "export $VAR=\"$VAL\""
}

function prefix_setup {
	local PFX="$1"

	add_export_env PATH "$PFX/bin"
	add_export_env LD_LIBRARY_PATH "$PFX/lib"
	add_export_env LD_RUN_PATH "$PFX/lib"
	add_export_env PKG_CONFIG_PATH "$PFX/lib/pkgconfig/" "$PFX/share/pkgconfig/"
	add_export_env MANPATH "$PFX/share/man"
	export ACLOCAL_PATH="$PFX/share/aclocal"
	export ACLOCAL="aclocal -I $ACLOCAL_PATH"
	mkdir -p "$ACLOCAL_PATH"
}

BASEDIR="$HOME/work/tmp/cairo-bench"
PFX="$BASEDIR/install"
mkdir -p "$PFX"
prefix_setup "$PFX"
