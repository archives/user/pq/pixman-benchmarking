% Analyze the nops test results

close all

global tests;
tests = {
	'over_n_8888'
	'over_n_0565'
	'over_8888_8_0565'
	'over_8888_n_0565'
	'over_8888_8888'
	'in_8888_8'
	'in_n_8888'
	'in_reverse_8888_8888'
	'src_1555_8888'
	'src_8888_8888'
	'src_0565_0565'
};

global fields;
fields = { 'L1' 'L2' 'M' 'HT' 'VT' 'R' 'RT' };

function data = load_data(testname, nopcount);
	fname = sprintf('../results-2/armv6l/20150626-nop-%d/%s.csv',
			nopcount, testname);

	data = load('-ascii', fname);
endfunction


function [mu sdev] = comp_stats(data);
	mu = mean(data, 1);
	sdev = std(data, 0, 1);
endfunction

function set_figure_sizes(fig, w, h);
	set(fig, 'paperunits', 'centimeters',
		'papersize', [w h],
		'paperposition', [0 0 w h]);

	pos = get(fig, 'position');
%	set(fig, 'position', [pos(1:2) (96 / 2.54 * [w h])]);

%	units = get(fig, 'units');
%	set(fig, 'units', get(fig, 'paperunits'));
%	set(fig, 'position', get(fig,'paperposition'));
%	set(fig, 'units', units);
endfunction

function out = add_trailing_space(str);
	if iscell(str)
		out = {};
		for k = 1:length(str)
			out = { out{:} [str{k} ' '] };
		endfor
	else
		out = [str ' '];
	endif
endfunction

function fig = analyze(testname, fldi);
	global fields;

	styles = { 'g' 'b' 'k' 'r' 'g:' 'b--' 'k:' };
	nopcnt = 0:16;
	mus = zeros(length(nopcnt), 7);
	sdevs = mus;

	for n = nopcnt
		data = load_data(testname, n);
		[mu sdev] = comp_stats(data);
		mus(n + 1, :) = mu;
		sdevs(n + 1, :) = sdev;
	endfor

	fig = figure(1);
	set(fig, 'defaultlinelinewidth', 4);
	for f = fldi
		% assuming normal distribution, the 95% range for samples
		erb = 1.96 * sdevs(:, f);

		errorbar(nopcnt, mus(:, f), erb, styles{f});
		hold on
	endfor
	set_figure_sizes(fig, 12, 9);
	%title(['operation: ' strrep(testname, '_', '\_')]);
	xlabel('number of nops');
	ylabel('Mpx/s');
	legend(add_trailing_space({fields{fldi}}), 'location', 'northeastoutside');
	axis([-1 17]);
	set(gca(), 'xtick', 0:4:16);

	hold off
endfunction

function export_image_file(testname);
	global fields;

	sets = { 1:7 };

	ttk = graphics_toolkit();
	graphics_toolkit('gnuplot');

	for k = 1:length(sets)
		fname = sprintf('fig-%s-%s.pdf',
				strrep(testname, '_', '-'),
				[ fields{sets{k}} ]);
		fig = analyze(testname, sets{k});
		print('-dpdf', '-r300', fname);
	endfor

	graphics_toolkit(ttk);
endfunction

function export_all_images;
	global tests;

	for k = 1:length(tests)
		export_image_file(tests{k});
	endfor
endfunction
