% Analyze the nops test results

close all

if exist('errorbar_tick', 'file') ~= 2
	addpath('errorbar_tick');
endif

global mus

function data = load_data(testname, nopcount);
	fname = sprintf('../results-2/armv7l/20150715-nop/20150715-nop-%d/%s.csv',
			nopcount, testname);

	data = load('-ascii', fname);
endfunction


function [mu sdev] = comp_stats(data);
	mu = mean(data, 1);
	sdev = std(data, 0, 1);
endfunction

function set_figure_sizes(fig, w, h);
	set(fig, 'paperunits', 'centimeters',
		'papersize', [w h],
		'paperposition', [0 0 w h]);

	pos = get(fig, 'position');
%	set(fig, 'position', [pos(1:2) (96 / 2.54 * [w h])]);

%	units = get(fig, 'units');
%	set(fig, 'units', get(fig, 'paperunits'));
%	set(fig, 'position', get(fig,'paperposition'));
%	set(fig, 'units', units);
endfunction

function fig = analyze(testname);
	global mus

	style = 'k';
	nopcnt = 0:256;
	mus = zeros(length(nopcnt), 1);
	sdevs = mus;

	for n = nopcnt
		data = load_data(testname, n);
		[mu sdev] = comp_stats(data);
		mus(n + 1, :) = mu;
		sdevs(n + 1, :) = sdev;
	endfor

	fig = figure(1);
	set(fig, 'defaultlinelinewidth', 4);

	% assuming normal distribution, the 95% range for samples
	erb = 1.96 * sdevs(:, 1);

	eb = errorbar(nopcnt, mus(:, 1), erb, style);
	errorbar_tick(eb, 1, 'units');
	hold on

	set_figure_sizes(fig, 36, 9);
	title('operation src\_8888\_8888, test L1, RPI2');
	xlabel('number of nops');
	ylabel('Mpx/s, 95% of normal distribution');
%	legend('L1 ', 'location', 'northeastoutside');
	axis([-1 257]);
	set(gca(), 'xtick', 0:8:256);
	grid on

	hold off
endfunction

function export_image_file(testname);
	ttk = graphics_toolkit();
	graphics_toolkit('gnuplot');

	fname = sprintf('fig-%s-L1-rpi2.pdf', strrep(testname, '_', '-'));
	fig = analyze(testname);
	print('-dpdf', '-r300', fname);

	graphics_toolkit(ttk);
endfunction

function export_all_images;
	export_image_file('src_8888_8888');
endfunction
