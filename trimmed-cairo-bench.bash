#!/bin/bash

BENCHDIR="$(readlink -en $(dirname $0))"

set -e

source "$BENCHDIR/cairo-env-setup.bash"

set -x

export CAIRO_TEST_TARGET=image
export CAIRO_TRACE_DIR=$HOME/work/git/trimmed-cairo-traces/benchmark
cairo-perf-trace -r -v -i8 > results-raw.txt
