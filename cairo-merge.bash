#!/bin/bash

function die {
	echo >&2
	echo "Error: $@" >&2
	exit 1
}

OUTFILE="$1"
shift

REFFILE="$1"
shift

cat "$REFFILE" > "$OUTFILE" || die "cat fail"

for F in "$@"
do
	diff -q <( cut -d' ' -f-4 "$REFFILE" ) <( cut -d' ' -f-4 "$F" ) || \
		die "$F test set differs"
	#echo "$REFFILE" "$F"
	paste -d' ' "$OUTFILE" <( cut -d' ' -f5- "$F" ) > tmp.tmp || \
		die "paste fail"
	cat tmp.tmp > "$OUTFILE" || die "cat fail"
done

rm tmp.tmp
