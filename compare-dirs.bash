#!/bin/bash

# https://github.com/bavison/perfcmp
PERFCMP=$HOME/git/perfcmp/perfcmp.py

DIR_A="$1"
DIR_B="$2"
shift
shift

basename -a $DIR_A/*.csv $DIR_B/*.csv | sort | uniq | \
while read fname
do
	$PERFCMP "$@" -t $(basename $fname .csv) $DIR_A/$fname $DIR_B/$fname
done
