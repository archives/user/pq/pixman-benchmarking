#!/bin/bash

# If '1', run 'make check' after build.
DO_CHECK=0

# If '1', run 'make distcheck' after build. Overrides DO_CHECK.
DO_DISTCHECK=0

#BLTBENCH="over_n_8888 over_n_0565 in_8888_8 over_8888_8_0565 over_8888_n_0565 in_n_8888 over_8888_8888 in_reverse_8888_8888 src_1555_8888 src_8888_8888 src_0565_0565"
BLTBENCH=""

# my intel laptop
BLTBENCH_SPEED=5850

# Raspberry Pi 1
#BLTBENCH_SPEED=676

MAKEOPTS="-j4"

BLTBENCH_ITERATIONS=2

#CAIRO_TRACE_DIR=$HOME/work/git/trimmed-cairo-traces/benchmark
CAIRO_TRACE_DIR=$HOME/git/pixman-benchmarking/traces
CAIRO_PERF_INTERNAL_ITERATIONS=8
CAIRO_PERF_EXTERNAL_ITERATIONS=6

#export PIXMAN_DISABLE=wholeops

function setup_revisions {
#	add_revision <codeversionname> <committish> [configureopts]

	# from the cover-benchmark-1 branch
	#add_revision 20151015-cover-clip-base dd968c4dd7170660530965977ed2555e77082006
	#add_revision 20151015-cover-clip-nomargin 00c460d6324848c3203e06e2ce75251427b2089c
	#add_revision 20151015-cover-clip-tight c064b17ff6576160327f42b289b2f4aa3e49f7e2
}

#---------------- end of configurable things -------------------

function die {
	echo >&2
	echo "PWD: $PWD" >&2
	echo "Error: $@" >&2
	exit 1
}

function nop {
	local dummy
}

if [[  "$TESTMODE" ]]; then
	P="nop"
	#set -x
else
	P=""
fi

BENCHDIR="$(readlink -en $(dirname $0))"
PIXMANCHKOUT="$(readlink -en .)"
BUILDCACHE="$BENCHDIR/cache"
ARCH="$(uname -m)"

declare -a NAMES REVS BUILDDIRS INSTALLDIRS RESULTDIRS CFGOPTIONS

function check_cpu_governor {
	local -i i=0
	local -i ret=0

	while [[ -e /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor ]]
	do
		if [[ $(< /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor ) != performance ]]
		then
			echo "CPU$i governor is not 'performance'!"
			ret=1
		fi
		(( i++ ))
	done

	return $ret
}

function add_revision {
	# args: name committish [configure options]
	local REV="$(git rev-parse --verify "$2")"|| \
		die "Bad git revision: $2"

	NAMES+=( "$1" )
	REVS+=( "$REV" )
	BUILDDIRS+=( "$BUILDCACHE/$REV-$1" )
	INSTALLDIRS+=( "$BUILDCACHE/$REV-$1/interbench-install" )
	RESULTDIRS+=( "$BENCHDIR/results-2/$ARCH/$1" )
	CFGOPTIONS+=( "$3" )
}

function print_revisions {
	# no args
	local i

	for ((i = 0; i < ${#NAMES[@]}; ++i))
	do
		printf '%20s: %s\n' "${NAMES[i]}" "${REVS[i]}"
		printf '%20s  %s\n' 'BUILD' "${BUILDDIRS[i]}"
		printf '%20s  %s\n' 'INSTALL' "${INSTALLDIRS[i]}"
		printf '%20s  %s\n' 'RESULT' "${RESULTDIRS[i]}"
		printf '%20s  %s\n' 'OPTS' "${CFGOPTIONS[i]}"
	done
	echo
}

function prep_result_dirs {
	# no args
	local i

	for ((i = 0; i < ${#NAMES[@]}; ++i))
	do
		[[ -d "${RESULTDIRS[i]}" ]] || mkdir -p "${RESULTDIRS[i]}" || \
			die "mkdir ${RESULTDIRS[i]} failed"

		uname -a > "${RESULTDIRS[i]}/uname-a.txt"

		(
			git describe --always "${REVS[i]}" && \
			echo && \
			git log -10 --format='%h %s (%ci)' "${REVS[i]}" && \
			echo && \
			echo "configure options: ${CFGOPTIONS[i]}"
		) &> "${RESULTDIRS[i]}/git-info.txt" \
			|| die "recording git info failed"
	done
}

function checkout {
	local REV="$1"

	cd "$PIXMANCHKOUT" || die "cd $PIXMANCHKOUT failed"
	git checkout "$REV" || die "git checkout $REV failed"
}

function build {
	local BUILDDIR="$1"
	local INSTALLDIR="$2"
	local OPTS="$3"

	mkdir -p "$BUILDDIR" || die "mkdir $BUILDDIR failed"
	mkdir -p "$INSTALLDIR" || die "mkdir $INSTALLDIR failed"
	cd "$BUILDDIR" || die "cd $BUILDDIR failed"

	"$PIXMANCHKOUT/configure" \
		--prefix="$INSTALLDIR" \
		--disable-gtk \
		--disable-libpng \
		$OPTS \
		|| die "configure failed"

	make $MAKEOPTS || die "make failed"
	make install || die "make install failed"

	if [[ "$DO_DISTCHECK" = 1 ]]; then
		make $MAKEOPTS distcheck || die "distcheck failed"
	elif [[ "$DO_CHECK" = 1 ]]; then
		make $MAKEOPTS check || die "make check failed"
	fi
}

function build_revisions {
	local i

	for ((i = 0; i < ${#NAMES[@]}; ++i))
	do
		echo
		echo '*********************************************'
		date '+%F %T'

		if [[ -e "${BUILDDIRS[i]}" ]]; then
			printf 'skip build %s: %s\n' "${NAMES[i]}" "${REVS[i]}"
			continue
		fi

		printf 'building %s: %s\n' "${NAMES[i]}" "${REVS[i]}"
		echo
		checkout "${REVS[i]}"
		build "${BUILDDIRS[i]}" "${INSTALLDIRS[i]}" "${CFGOPTIONS[i]}"
	done
}

function lowlevel_blt_bench {
	local DIR="$1"
	local PATTERN="$2"
	local RESULTFILE="$3"

	# If we hit SD-card or USB sync during a test, it will probably
	# ruin any measurements.
	sync

	# using libtool to execute handles LD_LIBRARY_PATH.

	"$DIR/libtool" --mode=execute \
	"$DIR/test/lowlevel-blt-bench" -c -m "$BLTBENCH_SPEED" "$PATTERN" \
		>> "$RESULTFILE" \
		|| die "lowlevel-blt-bench failed"
}

function run_bltbench_revisions {
	local PATTERN="$1"
	local i

	for ((i = 0; i < ${#NAMES[@]}; ++i))
	do
		#echo "${NAMES[i]}"
		lowlevel_blt_bench "${BUILDDIRS[i]}" "$PATTERN" \
			"${RESULTDIRS[i]}/${PATTERN}.csv"
	done
}

function run_cairo_perf {
	local INSTALLDIR="$1"
	local RESULTFILE="$2"

	# Here we do the complete environment setup
	source "$BENCHDIR/cairo-env-setup.bash" || die "cairo-env-setup fail"
	prefix_setup "$INSTALLDIR"
	export CAIRO_TRACE_DIR

	ldd $(which cairo-perf-trace) | grep -Ei 'pixman|cairo'
	sync
	export CAIRO_TEST_TARGET=image16
	cairo-perf-trace -r "-i$CAIRO_PERF_INTERNAL_ITERATIONS" \
		> "$RESULTFILE-16" || die "cairo-perf-trace failed"

	sync
	export CAIRO_TEST_TARGET=image
	cairo-perf-trace -r "-i$CAIRO_PERF_INTERNAL_ITERATIONS" \
		> "$RESULTFILE-32" || die "cairo-perf-trace failed"
}

function run_cairo_perf_revisions {
	local EXT_ROUND="$1"
	local i

	for ((i = 0; i < ${#NAMES[@]}; ++i))
	do
		echo "${NAMES[i]}"
		# sub-shell to maintain environment
		( run_cairo_perf "${INSTALLDIRS[i]}" \
			"${RESULTDIRS[i]}/cairo-perf-$EXT_ROUND.txt" )
	done
}

function merge_cairo_perf_results {
	local i

	for ((i = 0; i < ${#NAMES[@]}; ++i))
	do
		"$BENCHDIR"/cairo-merge.bash \
			"${RESULTDIRS[i]}/cairo-perf16.txt" \
			"${RESULTDIRS[i]}"/cairo-perf-*.txt-16

		"$BENCHDIR"/cairo-merge.bash \
			"${RESULTDIRS[i]}/cairo-perf32.txt" \
			"${RESULTDIRS[i]}"/cairo-perf-*.txt-32
	done
}


[[ -e pixman-1.pc.in && -e pixman/pixman.c ]] || \
	die "CWD must be a Pixman checkout"

echo "pixman-benchmarking checkout: $BENCHDIR"
echo "pixman checkout: $PIXMANCHKOUT"
echo "build cache: $BUILDCACHE"
echo

[[ -d "$BUILDCACHE" ]] || die "build cache dir does not exist"

echo "$(date '+%F %T') Begin."

if [[ ! -e configure ]]; then
	echo "Running autogen..."
	NOCONFIGURE=1 ./autogen.sh || die "autogen.sh failed"
fi

check_cpu_governor || die "fix your CPU governor"
setup_revisions
print_revisions
prep_result_dirs
build_revisions

for T in $BLTBENCH
do
	check_cpu_governor
	for ((i = 1; i <= $BLTBENCH_ITERATIONS; ++i))
	do
		echo "$(date '+%F %T') bltbench Iteration" \
			"${i}/${BLTBENCH_ITERATIONS} of ${T}..."
		run_bltbench_revisions "$T"
	done
done

for ((i = 1; i <= $CAIRO_PERF_EXTERNAL_ITERATIONS; ++i))
do
	check_cpu_governor
	echo "$(date '+%F %T') cairo-perf Iteration" \
		"${i}/${CAIRO_PERF_EXTERNAL_ITERATIONS}..."
	run_cairo_perf_revisions "$i"
done

merge_cairo_perf_results

echo "$(date '+%F %T') All done."
