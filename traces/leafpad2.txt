Recorded on rpi, on X
leafpad
manually built Pixman and Cairo
cairo: 1.12.16
pixman: pixman-0.32.0-30-g6bb1542

Opened Leafpad fairly big, resized a bit bigger.
Typed stuff, and a very long line. Jumped between begin and end of the long
line causing horz scrolling. Fiddled some menus, looked at the About dialog.
Quit.

