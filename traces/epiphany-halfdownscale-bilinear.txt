Recorded on rpi1, on X
epiphany-browser 3.8.2.0-0rpi21rpi1
manually built Cairo 1.14.2 for tracing
Pixman: 0.33.1+git20140627-c37ff5-rpi2rpi1

$ WEBKIT_DEFAULT_FILTER=bilinear cairo-trace --no-mark-dirty --no-callers epiphany-browser file://halfdownscale.html

Waited for the page to load, scrolled to the bottom, closed the window.
The web page contains images downscaled in x-direction only.
The code for halfdownscale.html is verbatim below:


<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>JPEGs</title>
    <style>
        h1 {
            font-size: large;
        }
    </style>
</head>
<body>
    <h1>JPEG decode test - half-downscaled</h1>
    <img width=1100 height=700 src="http://placecorgi.com/1200/700">
    <img width=1000 height=700 src="http://placecorgi.com/1100/700">
    <img width=1200 height=700 src="http://placecorgi.com/1300/700">
    <img width=900 height=700 src="http://placecorgi.com/1000/700">
    <img width=800  height=700 src="http://placecorgi.com/900/700">
    <img width=1400 height=700 src="http://placecorgi.com/1500/700">
</body>
</html>
