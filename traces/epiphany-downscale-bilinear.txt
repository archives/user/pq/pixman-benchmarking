Recorded on rpi1, on X
epiphany-browser 3.8.2.0-0rpi21rpi1
manually built Cairo 1.14.2 for tracing
Pixman: 0.33.1+git20140627-c37ff5-rpi2rpi1

$ WEBKIT_DEFAULT_FILTER=bilinear cairo-trace --no-mark-dirty --no-callers epiphany-browser http://raspberrypi.collabora.co.uk/tests/jpegs/downscaled.html

Waited for the page to load, closed the window.
This trivial artificial web page loads many images of size 0.9 - 1.5 Mpx,
presents them scaled down to 1/10th in both dimensions.

