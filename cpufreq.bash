#!/bin/bash

CTRL=/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

sudo bash -c "echo performance > $CTRL"

echo "cpufreq governor:"
cat "$CTRL"

echo 'Do not forget to check: cpupower -c all frequency-info'

