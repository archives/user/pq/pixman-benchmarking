\documentclass[a4paper]{article}

\usepackage[font=small,labelfont=bf,margin=1cm]{caption}[2006/01/12]


\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{subfig}
\usepackage{placeins} % for \FloatBarrier
\usepackage{graphicx}
\usepackage{fancyvrb}

% Ensure figure captions are below and table captions are above the content.
\usepackage{float}
\floatstyle{plain}\restylefloat{figure}
\floatstyle{plaintop}\restylefloat{table}

% additional document hyperref debug options: backref=page
\usepackage[pdfborder={0 0 0},plainpages=true]{hyperref}
%\usepackage[chapter]{algorithm}

%\hypersetup{colorlinks}
\hypersetup{bookmarksopen=true}

\graphicspath{{resources/}}

\title{Pixman benchmarking on Raspberry Pi 1: \\The nops case}
\author{Pekka Paalanen}
\date{30th June, 2015}

\makeatletter
\hypersetup{pdfauthor={\@author},pdftitle={\@title}}
\makeatother

\newcommand{\nopfigs}[2]{
	\begin{figure}[htp]
	\centering
	\includegraphics[width=0.98\textwidth]{fig-#1-L1L2MHTVTRRT}
	\caption{Operation #2.}
	\label{fig-#1}
	\end{figure}
}


\begin{document}

\maketitle

\section{Benchmark setup}

The following figures illustrate the effect of changing the alignment of
armv6 fast path functions in Pixman. The change in alignment was implemented
by adding the number of nop instructions before the function entry label,
and having a complementary set of nops after the function text to keep the
binary size constant. The base alignment before the nops was set to $9$ bits.

The tests were run with an interleaving benchmark harness, which means
that on each iteration it runs every code version sequentially. This spreads
out the executions of a particular binary over the whole duration of the
complete benchmark run. If there were any performance changes in the system
over long periods of time, this evens them out when comparing code versions
to each other.

The execution environment was \texttt{telinit 1}, meaning that no daemons
were running, just \texttt{init}, the shell, and the kernel tasks.
The network cable was unplugged to avoid the network stack waking up at
random times. The screen (VT) was forced to remain on, as screen blanking
causes changes to the memory bandwidth usage. Actively used storage
(for storing test binaries and results) and swap were on a USB HDD, with
the assumption that hitting the SD-card could be even more costly than the
USB HDD. All these procedures aimed to reduce random variations in
performance benchmark results.

The benchmarking program was Pixman's \texttt{lowlevel-blt-bench} modified to
print out machine readable results and to not measure the reference
\texttt{memcpy} speed. The speed was set at $676$~MB/s, which was a usual manual
measurement result. Each measurement (program execution) was repeated $30$ times
(the iteration count) to compute the mean and standard deviation. Due to a
misconfiguration, in\_8888\_8 and in\_n\_8888 have $60$ iterations instead of
$30$.

\section{Results}

The figures show the performance in megapixels per second against the number of
nops added before the function entry label. The error bars which in many cases
are too small to be clearly seen represent the $95\%$ range of the samples
assuming a normal distribution, that is, $\pm 1.96$ times the standard deviation
to the mean.

The entries L1, L2, M, HT, VT, R, and RT are different artificial work loads
\texttt{lowlevel-blt-bench} is using. L1 is processing tiny images that are
expected to fit in the L1 cache. L2 is processing small images that expected to
fit in L2 cache but not L1. M is doing operations with full-HD sized images. The
others are using various image tiling inspired schemes with sequential and
random (R, RT) offsets for small sub-image operations.

\nopfigs{over-n-8888}{over\_n\_8888}
\nopfigs{over-n-0565}{over\_n\_0565}
\nopfigs{over-8888-8-0565}{over\_8888\_8\_0565}
\nopfigs{over-8888-n-0565}{over\_8888\_n\_0565}
\nopfigs{over-8888-8888}{over\_8888\_8888}
\nopfigs{in-8888-8}{in\_8888\_8}
\nopfigs{in-n-8888}{in\_n\_8888}
\nopfigs{in-reverse-8888-8888}{in\_reverse\_8888\_8888}
\nopfigs{src-1555-8888}{src\_1555\_8888}
\nopfigs{src-8888-8888}{src\_8888\_8888}
\nopfigs{src-0565-0565}{src\_0565\_0565}

\FloatBarrier

\section{Discussion}

It is apparent that the number of nops has zero effect on practically all cases.
For some operations L2 shows greater variance than others, but still no effect
from nop count.  There are some peculiarities, though.

In Figure~\ref{fig-src-8888-8888} the last L1 point drop considerably for no
known reason. In Figures~\ref{fig-src-8888-8888} and~\ref{fig-src-0565-0565}
there are some changes in the L1 curves that might be statistically significant.

\section*{Appendix A: Disassembly}

The following disassembly around the over\_n\_8888 armv6 fast path shows
that the nop instructions are really there. This is the case of $7$ nops
before the function start.

\begin{Verbatim}[frame=lines,framesep=5mm,fontsize=\scriptsize,obeytabs=true,label={[Start of disassembly]End of disassembly}]
   6d8fc:	e1a00000 	nop			; (mov r0, r0)
	...
   6da00:	e1a00000 	nop			; (mov r0, r0)
   6da04:	e1a00000 	nop			; (mov r0, r0)
   6da08:	e1a00000 	nop			; (mov r0, r0)
   6da0c:	e1a00000 	nop			; (mov r0, r0)
   6da10:	e1a00000 	nop			; (mov r0, r0)
   6da14:	e1a00000 	nop			; (mov r0, r0)
   6da18:	e1a00000 	nop			; (mov r0, r0)

0006da1c <pixman_composite_over_n_8888_asm_armv6>:
   6da1c:	e92d4ff0 	push	{r4, r5, r6, r7, r8, r9, sl, fp, lr}
   6da20:	e2511001 	subs	r1, r1, #1
   6da24:	3a000210 	bcc	6e26c <pixman_composite_over_n_8888_asm_armv6+0x850>
   6da28:	e59d4024 	ldr	r4, [sp, #36]	; 0x24
   6da2c:	e59f64cc 	ldr	r6, [pc, #1228]	; 6df00 <pixman_composite_over_n_8888_asm_armv6+0x4e4>
   6da30:	e3a070ff 	mov	r7, #255	; 0xff
   6da34:	e0477c24 	sub	r7, r7, r4, lsr #24
   6da38:	e656cf96 	uadd8	ip, r6, r6
   6da3c:	e1a03103 	lsl	r3, r3, #2
   6da40:	e0433100 	sub	r3, r3, r0, lsl #2
   6da44:	e3500007 	cmp	r0, #7
   6da48:	3a0001b2 	bcc	6e118 <pixman_composite_over_n_8888_asm_armv6+0x6fc>
   6da4c:	e3500027 	cmp	r0, #39	; 0x27
   6da50:	3a00012b 	bcc	6df04 <pixman_composite_over_n_8888_asm_armv6+0x4e8>
   6da54:	e2400020 	sub	r0, r0, #32
   6da58:	e1a0e000 	mov	lr, r0
   6da5c:	e3c2b01f 	bic	fp, r2, #31
   6da60:	f5dbf000 	pld	[fp]
   6da64:	f5dbf020 	pld	[fp, #32]
   6da68:	f5dbf040 	pld	[fp, #64]	; 0x40
   6da6c:	e212800f 	ands	r8, r2, #15
   6da70:	0a000027 	beq	6db14 <pixman_composite_over_n_8888_asm_armv6+0xf8>
   6da74:	e2688010 	rsb	r8, r8, #16
   6da78:	e3120010 	tst	r2, #16
   6da7c:	0a000000 	beq	6da84 <pixman_composite_over_n_8888_asm_armv6+0x68>
   6da80:	f5dbf060 	pld	[fp, #96]	; 0x60
   6da84:	e1b0ce88 	lsls	ip, r8, #29
   6da88:	5a00000b 	bpl	6dabc <pixman_composite_over_n_8888_asm_armv6+0xa0>
   6da8c:	e4929004 	ldr	r9, [r2], #4
   6da90:	e2400001 	sub	r0, r0, #1
   6da94:	e6cfc079 	uxtb16	ip, r9
   6da98:	e6cf9479 	uxtb16	r9, r9, ror #8
   6da9c:	e02c679c 	mla	ip, ip, r7, r6
   6daa0:	e0296799 	mla	r9, r9, r7, r6
   6daa4:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6daa8:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6daac:	e1a0c46c 	ror	ip, ip, #8
   6dab0:	e68c9fb9 	sel	r9, ip, r9
   6dab4:	e6699f94 	uqadd8	r9, r9, r4
   6dab8:	e5029004 	str	r9, [r2, #-4]
   6dabc:	3a000014 	bcc	6db14 <pixman_composite_over_n_8888_asm_armv6+0xf8>
   6dac0:	e8b20c00 	ldm	r2!, {sl, fp}
   6dac4:	e2400002 	sub	r0, r0, #2
   6dac8:	e6cfc07a 	uxtb16	ip, sl
   6dacc:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6dad0:	e02c679c 	mla	ip, ip, r7, r6
   6dad4:	e02a679a 	mla	sl, sl, r7, r6
   6dad8:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dadc:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6dae0:	e1a0c46c 	ror	ip, ip, #8
   6dae4:	e68cafba 	sel	sl, ip, sl
   6dae8:	e66aaf94 	uqadd8	sl, sl, r4
   6daec:	e6cfc07b 	uxtb16	ip, fp
   6daf0:	e6cfb47b 	uxtb16	fp, fp, ror #8
   6daf4:	e02c679c 	mla	ip, ip, r7, r6
   6daf8:	e02b679b 	mla	fp, fp, r7, r6
   6dafc:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6db00:	e6cbb47b 	uxtab16	fp, fp, fp, ror #8
   6db04:	e1a0c46c 	ror	ip, ip, #8
   6db08:	e68cbfbb 	sel	fp, ip, fp
   6db0c:	e66bbf94 	uqadd8	fp, fp, r4
   6db10:	e9020c00 	stmdb	r2, {sl, fp}
   6db14:	e3120010 	tst	r2, #16
   6db18:	1a00004f 	bne	6dc5c <pixman_composite_over_n_8888_asm_armv6+0x240>
   6db1c:	e8b20f00 	ldm	r2!, {r8, r9, sl, fp}
   6db20:	f5d2f030 	pld	[r2, #48]	; 0x30
   6db24:	e6cfc078 	uxtb16	ip, r8
   6db28:	e6cf8478 	uxtb16	r8, r8, ror #8
   6db2c:	e02c679c 	mla	ip, ip, r7, r6
   6db30:	e0286798 	mla	r8, r8, r7, r6
   6db34:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6db38:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6db3c:	e1a0c46c 	ror	ip, ip, #8
   6db40:	e68c8fb8 	sel	r8, ip, r8
   6db44:	e6688f94 	uqadd8	r8, r8, r4
   6db48:	e6cfc079 	uxtb16	ip, r9
   6db4c:	e6cf9479 	uxtb16	r9, r9, ror #8
   6db50:	e02c679c 	mla	ip, ip, r7, r6
   6db54:	e0296799 	mla	r9, r9, r7, r6
   6db58:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6db5c:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6db60:	e1a0c46c 	ror	ip, ip, #8
   6db64:	e68c9fb9 	sel	r9, ip, r9
   6db68:	e6699f94 	uqadd8	r9, r9, r4
   6db6c:	e6cfc07a 	uxtb16	ip, sl
   6db70:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6db74:	e02c679c 	mla	ip, ip, r7, r6
   6db78:	e02a679a 	mla	sl, sl, r7, r6
   6db7c:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6db80:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6db84:	e1a0c46c 	ror	ip, ip, #8
   6db88:	e68cafba 	sel	sl, ip, sl
   6db8c:	e66aaf94 	uqadd8	sl, sl, r4
   6db90:	e6cfc07b 	uxtb16	ip, fp
   6db94:	e6cfb47b 	uxtb16	fp, fp, ror #8
   6db98:	e02c679c 	mla	ip, ip, r7, r6
   6db9c:	e02b679b 	mla	fp, fp, r7, r6
   6dba0:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dba4:	e6cbb47b 	uxtab16	fp, fp, fp, ror #8
   6dba8:	e1a0c46c 	ror	ip, ip, #8
   6dbac:	e68cbfbb 	sel	fp, ip, fp
   6dbb0:	e66bbf94 	uqadd8	fp, fp, r4
   6dbb4:	e9020f00 	stmdb	r2, {r8, r9, sl, fp}
   6dbb8:	e8b20f00 	ldm	r2!, {r8, r9, sl, fp}
   6dbbc:	e6cfc078 	uxtb16	ip, r8
   6dbc0:	e6cf8478 	uxtb16	r8, r8, ror #8
   6dbc4:	e02c679c 	mla	ip, ip, r7, r6
   6dbc8:	e0286798 	mla	r8, r8, r7, r6
   6dbcc:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dbd0:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6dbd4:	e1a0c46c 	ror	ip, ip, #8
   6dbd8:	e68c8fb8 	sel	r8, ip, r8
   6dbdc:	e6688f94 	uqadd8	r8, r8, r4
   6dbe0:	e6cfc079 	uxtb16	ip, r9
   6dbe4:	e6cf9479 	uxtb16	r9, r9, ror #8
   6dbe8:	e02c679c 	mla	ip, ip, r7, r6
   6dbec:	e0296799 	mla	r9, r9, r7, r6
   6dbf0:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dbf4:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6dbf8:	e1a0c46c 	ror	ip, ip, #8
   6dbfc:	e68c9fb9 	sel	r9, ip, r9
   6dc00:	e6699f94 	uqadd8	r9, r9, r4
   6dc04:	e6cfc07a 	uxtb16	ip, sl
   6dc08:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6dc0c:	e02c679c 	mla	ip, ip, r7, r6
   6dc10:	e02a679a 	mla	sl, sl, r7, r6
   6dc14:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dc18:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6dc1c:	e1a0c46c 	ror	ip, ip, #8
   6dc20:	e68cafba 	sel	sl, ip, sl
   6dc24:	e66aaf94 	uqadd8	sl, sl, r4
   6dc28:	e6cfc07b 	uxtb16	ip, fp
   6dc2c:	e6cfb47b 	uxtb16	fp, fp, ror #8
   6dc30:	e02c679c 	mla	ip, ip, r7, r6
   6dc34:	e02b679b 	mla	fp, fp, r7, r6
   6dc38:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dc3c:	e6cbb47b 	uxtab16	fp, fp, fp, ror #8
   6dc40:	e1a0c46c 	ror	ip, ip, #8
   6dc44:	e68cbfbb 	sel	fp, ip, fp
   6dc48:	e66bbf94 	uqadd8	fp, fp, r4
   6dc4c:	e9020f00 	stmdb	r2, {r8, r9, sl, fp}
   6dc50:	e2500008 	subs	r0, r0, #8
   6dc54:	2affffb0 	bcs	6db1c <pixman_composite_over_n_8888_asm_armv6+0x100>
   6dc58:	ea00004e 	b	6dd98 <pixman_composite_over_n_8888_asm_armv6+0x37c>
   6dc5c:	e8b20f00 	ldm	r2!, {r8, r9, sl, fp}
   6dc60:	f5d2f040 	pld	[r2, #64]	; 0x40
   6dc64:	e6cfc078 	uxtb16	ip, r8
   6dc68:	e6cf8478 	uxtb16	r8, r8, ror #8
   6dc6c:	e02c679c 	mla	ip, ip, r7, r6
   6dc70:	e0286798 	mla	r8, r8, r7, r6
   6dc74:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dc78:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6dc7c:	e1a0c46c 	ror	ip, ip, #8
   6dc80:	e68c8fb8 	sel	r8, ip, r8
   6dc84:	e6688f94 	uqadd8	r8, r8, r4
   6dc88:	e6cfc079 	uxtb16	ip, r9
   6dc8c:	e6cf9479 	uxtb16	r9, r9, ror #8
   6dc90:	e02c679c 	mla	ip, ip, r7, r6
   6dc94:	e0296799 	mla	r9, r9, r7, r6
   6dc98:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dc9c:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6dca0:	e1a0c46c 	ror	ip, ip, #8
   6dca4:	e68c9fb9 	sel	r9, ip, r9
   6dca8:	e6699f94 	uqadd8	r9, r9, r4
   6dcac:	e6cfc07a 	uxtb16	ip, sl
   6dcb0:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6dcb4:	e02c679c 	mla	ip, ip, r7, r6
   6dcb8:	e02a679a 	mla	sl, sl, r7, r6
   6dcbc:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dcc0:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6dcc4:	e1a0c46c 	ror	ip, ip, #8
   6dcc8:	e68cafba 	sel	sl, ip, sl
   6dccc:	e66aaf94 	uqadd8	sl, sl, r4
   6dcd0:	e6cfc07b 	uxtb16	ip, fp
   6dcd4:	e6cfb47b 	uxtb16	fp, fp, ror #8
   6dcd8:	e02c679c 	mla	ip, ip, r7, r6
   6dcdc:	e02b679b 	mla	fp, fp, r7, r6
   6dce0:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dce4:	e6cbb47b 	uxtab16	fp, fp, fp, ror #8
   6dce8:	e1a0c46c 	ror	ip, ip, #8
   6dcec:	e68cbfbb 	sel	fp, ip, fp
   6dcf0:	e66bbf94 	uqadd8	fp, fp, r4
   6dcf4:	e9020f00 	stmdb	r2, {r8, r9, sl, fp}
   6dcf8:	e8b20f00 	ldm	r2!, {r8, r9, sl, fp}
   6dcfc:	e6cfc078 	uxtb16	ip, r8
   6dd00:	e6cf8478 	uxtb16	r8, r8, ror #8
   6dd04:	e02c679c 	mla	ip, ip, r7, r6
   6dd08:	e0286798 	mla	r8, r8, r7, r6
   6dd0c:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dd10:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6dd14:	e1a0c46c 	ror	ip, ip, #8
   6dd18:	e68c8fb8 	sel	r8, ip, r8
   6dd1c:	e6688f94 	uqadd8	r8, r8, r4
   6dd20:	e6cfc079 	uxtb16	ip, r9
   6dd24:	e6cf9479 	uxtb16	r9, r9, ror #8
   6dd28:	e02c679c 	mla	ip, ip, r7, r6
   6dd2c:	e0296799 	mla	r9, r9, r7, r6
   6dd30:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dd34:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6dd38:	e1a0c46c 	ror	ip, ip, #8
   6dd3c:	e68c9fb9 	sel	r9, ip, r9
   6dd40:	e6699f94 	uqadd8	r9, r9, r4
   6dd44:	e6cfc07a 	uxtb16	ip, sl
   6dd48:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6dd4c:	e02c679c 	mla	ip, ip, r7, r6
   6dd50:	e02a679a 	mla	sl, sl, r7, r6
   6dd54:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dd58:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6dd5c:	e1a0c46c 	ror	ip, ip, #8
   6dd60:	e68cafba 	sel	sl, ip, sl
   6dd64:	e66aaf94 	uqadd8	sl, sl, r4
   6dd68:	e6cfc07b 	uxtb16	ip, fp
   6dd6c:	e6cfb47b 	uxtb16	fp, fp, ror #8
   6dd70:	e02c679c 	mla	ip, ip, r7, r6
   6dd74:	e02b679b 	mla	fp, fp, r7, r6
   6dd78:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dd7c:	e6cbb47b 	uxtab16	fp, fp, fp, ror #8
   6dd80:	e1a0c46c 	ror	ip, ip, #8
   6dd84:	e68cbfbb 	sel	fp, ip, fp
   6dd88:	e66bbf94 	uqadd8	fp, fp, r4
   6dd8c:	e9020f00 	stmdb	r2, {r8, r9, sl, fp}
   6dd90:	e2500008 	subs	r0, r0, #8
   6dd94:	2affffb0 	bcs	6dc5c <pixman_composite_over_n_8888_asm_armv6+0x240>
   6dd98:	e1a0cd82 	lsl	ip, r2, #27
   6dd9c:	e09cce80 	adds	ip, ip, r0, lsl #29
   6dda0:	02bcc000 	adcseq	ip, ip, #0
   6dda4:	0a000003 	beq	6ddb8 <pixman_composite_over_n_8888_asm_armv6+0x39c>
   6dda8:	e3c2c01f 	bic	ip, r2, #31
   6ddac:	3a000000 	bcc	6ddb4 <pixman_composite_over_n_8888_asm_armv6+0x398>
   6ddb0:	f5dcf080 	pld	[ip, #128]	; 0x80
   6ddb4:	f5dcf060 	pld	[ip, #96]	; 0x60
   6ddb8:	e280001c 	add	r0, r0, #28
   6ddbc:	e8b20f00 	ldm	r2!, {r8, r9, sl, fp}
   6ddc0:	e6cfc078 	uxtb16	ip, r8
   6ddc4:	e6cf8478 	uxtb16	r8, r8, ror #8
   6ddc8:	e02c679c 	mla	ip, ip, r7, r6
   6ddcc:	e0286798 	mla	r8, r8, r7, r6
   6ddd0:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6ddd4:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6ddd8:	e1a0c46c 	ror	ip, ip, #8
   6dddc:	e68c8fb8 	sel	r8, ip, r8
   6dde0:	e6688f94 	uqadd8	r8, r8, r4
   6dde4:	e6cfc079 	uxtb16	ip, r9
   6dde8:	e6cf9479 	uxtb16	r9, r9, ror #8
   6ddec:	e02c679c 	mla	ip, ip, r7, r6
   6ddf0:	e0296799 	mla	r9, r9, r7, r6
   6ddf4:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6ddf8:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6ddfc:	e1a0c46c 	ror	ip, ip, #8
   6de00:	e68c9fb9 	sel	r9, ip, r9
   6de04:	e6699f94 	uqadd8	r9, r9, r4
   6de08:	e6cfc07a 	uxtb16	ip, sl
   6de0c:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6de10:	e02c679c 	mla	ip, ip, r7, r6
   6de14:	e02a679a 	mla	sl, sl, r7, r6
   6de18:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6de1c:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6de20:	e1a0c46c 	ror	ip, ip, #8
   6de24:	e68cafba 	sel	sl, ip, sl
   6de28:	e66aaf94 	uqadd8	sl, sl, r4
   6de2c:	e6cfc07b 	uxtb16	ip, fp
   6de30:	e6cfb47b 	uxtb16	fp, fp, ror #8
   6de34:	e02c679c 	mla	ip, ip, r7, r6
   6de38:	e02b679b 	mla	fp, fp, r7, r6
   6de3c:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6de40:	e6cbb47b 	uxtab16	fp, fp, fp, ror #8
   6de44:	e1a0c46c 	ror	ip, ip, #8
   6de48:	e68cbfbb 	sel	fp, ip, fp
   6de4c:	e66bbf94 	uqadd8	fp, fp, r4
   6de50:	e9020f00 	stmdb	r2, {r8, r9, sl, fp}
   6de54:	e2500004 	subs	r0, r0, #4
   6de58:	2affffd7 	bcs	6ddbc <pixman_composite_over_n_8888_asm_armv6+0x3a0>
   6de5c:	e3100003 	tst	r0, #3
   6de60:	0a000021 	beq	6deec <pixman_composite_over_n_8888_asm_armv6+0x4d0>
   6de64:	e1b0cf80 	lsls	ip, r0, #31
   6de68:	3a000013 	bcc	6debc <pixman_composite_over_n_8888_asm_armv6+0x4a0>
   6de6c:	e8b20300 	ldm	r2!, {r8, r9}
   6de70:	e6cfc078 	uxtb16	ip, r8
   6de74:	e6cf8478 	uxtb16	r8, r8, ror #8
   6de78:	e02c679c 	mla	ip, ip, r7, r6
   6de7c:	e0286798 	mla	r8, r8, r7, r6
   6de80:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6de84:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6de88:	e1a0c46c 	ror	ip, ip, #8
   6de8c:	e68c8fb8 	sel	r8, ip, r8
   6de90:	e6688f94 	uqadd8	r8, r8, r4
   6de94:	e6cfc079 	uxtb16	ip, r9
   6de98:	e6cf9479 	uxtb16	r9, r9, ror #8
   6de9c:	e02c679c 	mla	ip, ip, r7, r6
   6dea0:	e0296799 	mla	r9, r9, r7, r6
   6dea4:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dea8:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6deac:	e1a0c46c 	ror	ip, ip, #8
   6deb0:	e68c9fb9 	sel	r9, ip, r9
   6deb4:	e6699f94 	uqadd8	r9, r9, r4
   6deb8:	e9020300 	stmdb	r2, {r8, r9}
   6debc:	5a00000a 	bpl	6deec <pixman_composite_over_n_8888_asm_armv6+0x4d0>
   6dec0:	e492a004 	ldr	sl, [r2], #4
   6dec4:	e6cfc07a 	uxtb16	ip, sl
   6dec8:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6decc:	e02c679c 	mla	ip, ip, r7, r6
   6ded0:	e02a679a 	mla	sl, sl, r7, r6
   6ded4:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6ded8:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6dedc:	e1a0c46c 	ror	ip, ip, #8
   6dee0:	e68cafba 	sel	sl, ip, sl
   6dee4:	e66aaf94 	uqadd8	sl, sl, r4
   6dee8:	e502a004 	str	sl, [r2, #-4]
   6deec:	e2511001 	subs	r1, r1, #1
   6def0:	e0822003 	add	r2, r2, r3
   6def4:	e1a0000e 	mov	r0, lr
   6def8:	2afffed7 	bcs	6da5c <pixman_composite_over_n_8888_asm_armv6+0x40>
   6defc:	ea0000da 	b	6e26c <pixman_composite_over_n_8888_asm_armv6+0x850>
   6df00:	00800080 	.word	0x00800080
   6df04:	e1a0e000 	mov	lr, r0
   6df08:	e3c2801f 	bic	r8, r2, #31
   6df0c:	f5d8f000 	pld	[r8]
   6df10:	e0829100 	add	r9, r2, r0, lsl #2
   6df14:	e2499001 	sub	r9, r9, #1
   6df18:	e3c9901f 	bic	r9, r9, #31
   6df1c:	e1590008 	cmp	r9, r8
   6df20:	0a000003 	beq	6df34 <pixman_composite_over_n_8888_asm_armv6+0x518>
   6df24:	e2888020 	add	r8, r8, #32
   6df28:	e1580009 	cmp	r8, r9
   6df2c:	f5d8f000 	pld	[r8]
   6df30:	1afffffb 	bne	6df24 <pixman_composite_over_n_8888_asm_armv6+0x508>
   6df34:	e2400004 	sub	r0, r0, #4
   6df38:	e212800f 	ands	r8, r2, #15
   6df3c:	0a000024 	beq	6dfd4 <pixman_composite_over_n_8888_asm_armv6+0x5b8>
   6df40:	e2688010 	rsb	r8, r8, #16
   6df44:	e1b0ce88 	lsls	ip, r8, #29
   6df48:	5a00000b 	bpl	6df7c <pixman_composite_over_n_8888_asm_armv6+0x560>
   6df4c:	e4929004 	ldr	r9, [r2], #4
   6df50:	e2400001 	sub	r0, r0, #1
   6df54:	e6cfc079 	uxtb16	ip, r9
   6df58:	e6cf9479 	uxtb16	r9, r9, ror #8
   6df5c:	e02c679c 	mla	ip, ip, r7, r6
   6df60:	e0296799 	mla	r9, r9, r7, r6
   6df64:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6df68:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6df6c:	e1a0c46c 	ror	ip, ip, #8
   6df70:	e68c9fb9 	sel	r9, ip, r9
   6df74:	e6699f94 	uqadd8	r9, r9, r4
   6df78:	e5029004 	str	r9, [r2, #-4]
   6df7c:	3a000014 	bcc	6dfd4 <pixman_composite_over_n_8888_asm_armv6+0x5b8>
   6df80:	e8b20c00 	ldm	r2!, {sl, fp}
   6df84:	e2400002 	sub	r0, r0, #2
   6df88:	e6cfc07a 	uxtb16	ip, sl
   6df8c:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6df90:	e02c679c 	mla	ip, ip, r7, r6
   6df94:	e02a679a 	mla	sl, sl, r7, r6
   6df98:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6df9c:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6dfa0:	e1a0c46c 	ror	ip, ip, #8
   6dfa4:	e68cafba 	sel	sl, ip, sl
   6dfa8:	e66aaf94 	uqadd8	sl, sl, r4
   6dfac:	e6cfc07b 	uxtb16	ip, fp
   6dfb0:	e6cfb47b 	uxtb16	fp, fp, ror #8
   6dfb4:	e02c679c 	mla	ip, ip, r7, r6
   6dfb8:	e02b679b 	mla	fp, fp, r7, r6
   6dfbc:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dfc0:	e6cbb47b 	uxtab16	fp, fp, fp, ror #8
   6dfc4:	e1a0c46c 	ror	ip, ip, #8
   6dfc8:	e68cbfbb 	sel	fp, ip, fp
   6dfcc:	e66bbf94 	uqadd8	fp, fp, r4
   6dfd0:	e9020c00 	stmdb	r2, {sl, fp}
   6dfd4:	e8b20f00 	ldm	r2!, {r8, r9, sl, fp}
   6dfd8:	e6cfc078 	uxtb16	ip, r8
   6dfdc:	e6cf8478 	uxtb16	r8, r8, ror #8
   6dfe0:	e02c679c 	mla	ip, ip, r7, r6
   6dfe4:	e0286798 	mla	r8, r8, r7, r6
   6dfe8:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6dfec:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6dff0:	e1a0c46c 	ror	ip, ip, #8
   6dff4:	e68c8fb8 	sel	r8, ip, r8
   6dff8:	e6688f94 	uqadd8	r8, r8, r4
   6dffc:	e6cfc079 	uxtb16	ip, r9
   6e000:	e6cf9479 	uxtb16	r9, r9, ror #8
   6e004:	e02c679c 	mla	ip, ip, r7, r6
   6e008:	e0296799 	mla	r9, r9, r7, r6
   6e00c:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e010:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6e014:	e1a0c46c 	ror	ip, ip, #8
   6e018:	e68c9fb9 	sel	r9, ip, r9
   6e01c:	e6699f94 	uqadd8	r9, r9, r4
   6e020:	e6cfc07a 	uxtb16	ip, sl
   6e024:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6e028:	e02c679c 	mla	ip, ip, r7, r6
   6e02c:	e02a679a 	mla	sl, sl, r7, r6
   6e030:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e034:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6e038:	e1a0c46c 	ror	ip, ip, #8
   6e03c:	e68cafba 	sel	sl, ip, sl
   6e040:	e66aaf94 	uqadd8	sl, sl, r4
   6e044:	e6cfc07b 	uxtb16	ip, fp
   6e048:	e6cfb47b 	uxtb16	fp, fp, ror #8
   6e04c:	e02c679c 	mla	ip, ip, r7, r6
   6e050:	e02b679b 	mla	fp, fp, r7, r6
   6e054:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e058:	e6cbb47b 	uxtab16	fp, fp, fp, ror #8
   6e05c:	e1a0c46c 	ror	ip, ip, #8
   6e060:	e68cbfbb 	sel	fp, ip, fp
   6e064:	e66bbf94 	uqadd8	fp, fp, r4
   6e068:	e9020f00 	stmdb	r2, {r8, r9, sl, fp}
   6e06c:	e2500004 	subs	r0, r0, #4
   6e070:	2affffd7 	bcs	6dfd4 <pixman_composite_over_n_8888_asm_armv6+0x5b8>
   6e074:	e3100003 	tst	r0, #3
   6e078:	0a000021 	beq	6e104 <pixman_composite_over_n_8888_asm_armv6+0x6e8>
   6e07c:	e1b0cf80 	lsls	ip, r0, #31
   6e080:	3a000013 	bcc	6e0d4 <pixman_composite_over_n_8888_asm_armv6+0x6b8>
   6e084:	e8b20300 	ldm	r2!, {r8, r9}
   6e088:	e6cfc078 	uxtb16	ip, r8
   6e08c:	e6cf8478 	uxtb16	r8, r8, ror #8
   6e090:	e02c679c 	mla	ip, ip, r7, r6
   6e094:	e0286798 	mla	r8, r8, r7, r6
   6e098:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e09c:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6e0a0:	e1a0c46c 	ror	ip, ip, #8
   6e0a4:	e68c8fb8 	sel	r8, ip, r8
   6e0a8:	e6688f94 	uqadd8	r8, r8, r4
   6e0ac:	e6cfc079 	uxtb16	ip, r9
   6e0b0:	e6cf9479 	uxtb16	r9, r9, ror #8
   6e0b4:	e02c679c 	mla	ip, ip, r7, r6
   6e0b8:	e0296799 	mla	r9, r9, r7, r6
   6e0bc:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e0c0:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6e0c4:	e1a0c46c 	ror	ip, ip, #8
   6e0c8:	e68c9fb9 	sel	r9, ip, r9
   6e0cc:	e6699f94 	uqadd8	r9, r9, r4
   6e0d0:	e9020300 	stmdb	r2, {r8, r9}
   6e0d4:	5a00000a 	bpl	6e104 <pixman_composite_over_n_8888_asm_armv6+0x6e8>
   6e0d8:	e492a004 	ldr	sl, [r2], #4
   6e0dc:	e6cfc07a 	uxtb16	ip, sl
   6e0e0:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6e0e4:	e02c679c 	mla	ip, ip, r7, r6
   6e0e8:	e02a679a 	mla	sl, sl, r7, r6
   6e0ec:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e0f0:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6e0f4:	e1a0c46c 	ror	ip, ip, #8
   6e0f8:	e68cafba 	sel	sl, ip, sl
   6e0fc:	e66aaf94 	uqadd8	sl, sl, r4
   6e100:	e502a004 	str	sl, [r2, #-4]
   6e104:	e2511001 	subs	r1, r1, #1
   6e108:	e0822003 	add	r2, r2, r3
   6e10c:	e1a0000e 	mov	r0, lr
   6e110:	2affff7c 	bcs	6df08 <pixman_composite_over_n_8888_asm_armv6+0x4ec>
   6e114:	ea000054 	b	6e26c <pixman_composite_over_n_8888_asm_armv6+0x850>
   6e118:	e3c2801f 	bic	r8, r2, #31
   6e11c:	f5d8f000 	pld	[r8]
   6e120:	e0829100 	add	r9, r2, r0, lsl #2
   6e124:	e2499001 	sub	r9, r9, #1
   6e128:	e3c9901f 	bic	r9, r9, #31
   6e12c:	e1590008 	cmp	r9, r8
   6e130:	0a000000 	beq	6e138 <pixman_composite_over_n_8888_asm_armv6+0x71c>
   6e134:	f5d9f000 	pld	[r9]
   6e138:	e3100004 	tst	r0, #4
   6e13c:	0a000025 	beq	6e1d8 <pixman_composite_over_n_8888_asm_armv6+0x7bc>
   6e140:	e8b20f00 	ldm	r2!, {r8, r9, sl, fp}
   6e144:	e6cfc078 	uxtb16	ip, r8
   6e148:	e6cf8478 	uxtb16	r8, r8, ror #8
   6e14c:	e02c679c 	mla	ip, ip, r7, r6
   6e150:	e0286798 	mla	r8, r8, r7, r6
   6e154:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e158:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6e15c:	e1a0c46c 	ror	ip, ip, #8
   6e160:	e68c8fb8 	sel	r8, ip, r8
   6e164:	e6688f94 	uqadd8	r8, r8, r4
   6e168:	e6cfc079 	uxtb16	ip, r9
   6e16c:	e6cf9479 	uxtb16	r9, r9, ror #8
   6e170:	e02c679c 	mla	ip, ip, r7, r6
   6e174:	e0296799 	mla	r9, r9, r7, r6
   6e178:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e17c:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6e180:	e1a0c46c 	ror	ip, ip, #8
   6e184:	e68c9fb9 	sel	r9, ip, r9
   6e188:	e6699f94 	uqadd8	r9, r9, r4
   6e18c:	e6cfc07a 	uxtb16	ip, sl
   6e190:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6e194:	e02c679c 	mla	ip, ip, r7, r6
   6e198:	e02a679a 	mla	sl, sl, r7, r6
   6e19c:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e1a0:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6e1a4:	e1a0c46c 	ror	ip, ip, #8
   6e1a8:	e68cafba 	sel	sl, ip, sl
   6e1ac:	e66aaf94 	uqadd8	sl, sl, r4
   6e1b0:	e6cfc07b 	uxtb16	ip, fp
   6e1b4:	e6cfb47b 	uxtb16	fp, fp, ror #8
   6e1b8:	e02c679c 	mla	ip, ip, r7, r6
   6e1bc:	e02b679b 	mla	fp, fp, r7, r6
   6e1c0:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e1c4:	e6cbb47b 	uxtab16	fp, fp, fp, ror #8
   6e1c8:	e1a0c46c 	ror	ip, ip, #8
   6e1cc:	e68cbfbb 	sel	fp, ip, fp
   6e1d0:	e66bbf94 	uqadd8	fp, fp, r4
   6e1d4:	e9020f00 	stmdb	r2, {r8, r9, sl, fp}
   6e1d8:	e1b0cf80 	lsls	ip, r0, #31
   6e1dc:	3a000013 	bcc	6e230 <pixman_composite_over_n_8888_asm_armv6+0x814>
   6e1e0:	e8b20300 	ldm	r2!, {r8, r9}
   6e1e4:	e6cfc078 	uxtb16	ip, r8
   6e1e8:	e6cf8478 	uxtb16	r8, r8, ror #8
   6e1ec:	e02c679c 	mla	ip, ip, r7, r6
   6e1f0:	e0286798 	mla	r8, r8, r7, r6
   6e1f4:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e1f8:	e6c88478 	uxtab16	r8, r8, r8, ror #8
   6e1fc:	e1a0c46c 	ror	ip, ip, #8
   6e200:	e68c8fb8 	sel	r8, ip, r8
   6e204:	e6688f94 	uqadd8	r8, r8, r4
   6e208:	e6cfc079 	uxtb16	ip, r9
   6e20c:	e6cf9479 	uxtb16	r9, r9, ror #8
   6e210:	e02c679c 	mla	ip, ip, r7, r6
   6e214:	e0296799 	mla	r9, r9, r7, r6
   6e218:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e21c:	e6c99479 	uxtab16	r9, r9, r9, ror #8
   6e220:	e1a0c46c 	ror	ip, ip, #8
   6e224:	e68c9fb9 	sel	r9, ip, r9
   6e228:	e6699f94 	uqadd8	r9, r9, r4
   6e22c:	e9020300 	stmdb	r2, {r8, r9}
   6e230:	5a00000a 	bpl	6e260 <pixman_composite_over_n_8888_asm_armv6+0x844>
   6e234:	e492a004 	ldr	sl, [r2], #4
   6e238:	e6cfc07a 	uxtb16	ip, sl
   6e23c:	e6cfa47a 	uxtb16	sl, sl, ror #8
   6e240:	e02c679c 	mla	ip, ip, r7, r6
   6e244:	e02a679a 	mla	sl, sl, r7, r6
   6e248:	e6ccc47c 	uxtab16	ip, ip, ip, ror #8
   6e24c:	e6caa47a 	uxtab16	sl, sl, sl, ror #8
   6e250:	e1a0c46c 	ror	ip, ip, #8
   6e254:	e68cafba 	sel	sl, ip, sl
   6e258:	e66aaf94 	uqadd8	sl, sl, r4
   6e25c:	e502a004 	str	sl, [r2, #-4]
   6e260:	e2511001 	subs	r1, r1, #1
   6e264:	e0822003 	add	r2, r2, r3
   6e268:	2affffaa 	bcs	6e118 <pixman_composite_over_n_8888_asm_armv6+0x6fc>
   6e26c:	e8bd8ff0 	pop	{r4, r5, r6, r7, r8, r9, sl, fp, pc}
   6e270:	e1a00000 	nop			; (mov r0, r0)
   6e274:	e1a00000 	nop			; (mov r0, r0)
   6e278:	e1a00000 	nop			; (mov r0, r0)
   6e27c:	e1a00000 	nop			; (mov r0, r0)
   6e280:	e1a00000 	nop			; (mov r0, r0)
   6e284:	e1a00000 	nop			; (mov r0, r0)
   6e288:	e1a00000 	nop			; (mov r0, r0)
   6e28c:	e1a00000 	nop			; (mov r0, r0)
   6e290:	e1a00000 	nop			; (mov r0, r0)
   6e294:	e1a00000 	nop			; (mov r0, r0)
   6e298:	e1a00000 	nop			; (mov r0, r0)
   6e29c:	e1a00000 	nop			; (mov r0, r0)
   6e2a0:	e1a00000 	nop			; (mov r0, r0)
   6e2a4:	e1a00000 	nop			; (mov r0, r0)
   6e2a8:	e1a00000 	nop			; (mov r0, r0)
   6e2ac:	e1a00000 	nop			; (mov r0, r0)
   6e2b0:	e1a00000 	nop			; (mov r0, r0)
   6e2b4:	e1a00000 	nop			; (mov r0, r0)
   6e2b8:	e1a00000 	nop			; (mov r0, r0)
   6e2bc:	e1a00000 	nop			; (mov r0, r0)
	...
   6e400:	e1a00000 	nop			; (mov r0, r0)
   6e404:	e1a00000 	nop			; (mov r0, r0)
   6e408:	e1a00000 	nop			; (mov r0, r0)
   6e40c:	e1a00000 	nop			; (mov r0, r0)
   6e410:	e1a00000 	nop			; (mov r0, r0)
   6e414:	e1a00000 	nop			; (mov r0, r0)
   6e418:	e1a00000 	nop			; (mov r0, r0)

0006e41c <pixman_composite_over_n_0565_asm_armv6>:
   6e41c:	e92d4ff0 	push	{r4, r5, r6, r7, r8, r9, sl, fp, lr}
\end{Verbatim}


\end{document}
